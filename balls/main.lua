up = 0
math.randomseed(os.clock())

down = 0
left = 0
right = 0
width = 1000
height = 1000
dt = nil
x_ellipse = width/2
y_ellipse = height/2
x_update = 0
y_update = 0

fps=240
base_speed_factor = 1024
speed = base_speed_factor/fps
print(x_ellipse, y_ellipse, math.random())
radius = 30
field = 450
ellipses = {}

function rand_sign() signs={-1,1}; return signs[math.random(2)] end

function love.load()
   local f = love.graphics.newFont(12)
   love.graphics.setFont(f)
   love.graphics.setColor(0,0,0,255)
   love.graphics.setBackgroundColor(255,255,255)
   love.window.setMode( width, height, {vsync=1,msaa=16,depth=24,borderless=true} )
end

function love.update(dt)
  if love.keyboard.isDown("c")
  then
    xy_factor = math.random()
    field = 450
    table.insert(ellipses, {rgb={math.random(), math.random(), math.random()} ,
                           x = math.random(field+radius,width-(field+radius)), 
                           y = math.random(field+radius,height-(field+radius)), 
                           x_inc =  xy_factor * rand_sign(), 
                           y_inc =  (1-xy_factor) * rand_sign()})
  end

  if love.keyboard.isDown("r")
  then
    ellipses = {}
  end

  if love.keyboard.isDown("q")
  then
    love.event.quit()
  end

  if love.keyboard.isDown("f")
  then
    field = 0
  end


  for ellipse in pairs(ellipses)
  do
    ellipse = ellipses[ellipse]
    if ellipse.x <= (field) or ellipse.x  >= width-(field+radius)
    then
      ellipse.x_inc = ellipse.x_inc * (-1)
    end

    if ellipse.y <= (field) or ellipse.y  >= height-(field+radius)
    then
      ellipse.y_inc = ellipse.y_inc * (-1)
    end
    ellipse.x = ellipse.x + (ellipse.x_inc * speed)
    ellipse.y = ellipse.y + (ellipse.y_inc * speed)

  end
end

function love.draw()
      love.timer.sleep(1/fps)
      love.graphics.setColor(0,0,0)
      love.graphics.print(x_ellipse..' '..y_ellipse, 0, 0)
      love.graphics.print(x_update..' '..y_update, 0, 10)
      love.graphics.rectangle('line', field, field, width-(field*2), height-(field*2))
      for ellipse in pairs(ellipses)
      do
        ellipse = ellipses[ellipse]
        love.graphics.setColor(ellipse.rgb[1], ellipse.rgb[2], ellipse.rgb[3])
        love.graphics.rectangle( 'fill', ellipse.x, ellipse.y, radius, radius ,4)
        
      end
end



















